Updating for a new core minor version
=====================================

1. Check the version of CKEditor used in the core minor version by looking in core/assets/vendor/ckeditor/ckeditor.js

2. Edit core-versions.txt and put the CKEditor version, followed by a composer contraint for the Drupal core version, ex. "4.14.0: ~8.8.0"

3. Run `./generate.php` to update the satis.json file

4. Run `./build.sh` to generate the `output/` directory

5. Open `output/index.html` in a web browser and verify that everything is looking OK

6. Commit and push the changes which will rebuild the public repository in GitLab CI

